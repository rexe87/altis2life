/*
	File: fn_welcomeNotification.sqf
	
	Description:
	Called upon first spawn selection and welcomes our player.
*/
format["Hallo %1 !",name player] hintC
[
	"Willkommen auf dem Altis Life RPG Server von ARG!",
	"Die neue Standard-Aktionstaste ist [LINKS Windows], diese Belegung kannst du unter ESC ->Configure->Controls->Custom
	'Use Action 10' beliebig aendern.",
	"Wenn du Probleme mit den neuen Funktionen hast, wende dich an einen Admin (TS: 78.31.70.230:9987 oder melde dich im Forum unter www.altis-roleplay-gaming.de",
	"Nun viel Spass beim spielen! Immer schoen an die Regeln halten!",
	"Wirbt Spieler! Jeder der geworben wurde wird belohnt! Hierzu einfach eine PN an Rexee87!"
];
	